from pynput.keyboard import Controller
import PyQt5.QtWidgets
from PyQt5.QtWidgets import QWidget, QPushButton, QLineEdit, QGridLayout, QLabel, QMessageBox
from PyQt5.QtGui import QIntValidator
from PyQt5.QtCore import QTimer
import time

kbd = Controller()

class AppWidget(QWidget):

    timer = QTimer()

    def __init__(self):

        QWidget.__init__(self)

        self.setFixedHeight(250)
        self.setFixedWidth(300)

        self.setObjectName("MainWidget")

        gridLayout = QGridLayout(self)

        self.setLayout(gridLayout)

        self.setWindowTitle("Maxters - Auto-keyboard")

        self.intervalLabel = QLabel("Intervalo", self)
        self.intervalInput = QLineEdit(self)
        self.intervalInput.setValidator(QIntValidator())

        self.valueLabel = QLabel("Insira um valor")
        self.valueInput = QLineEdit(self)
        

        self.buttonRun = QPushButton("Iniciar", self)

        gridLayout.addWidget(self.intervalLabel, 0, 0)
        gridLayout.addWidget(self.intervalInput, 1, 0)

        gridLayout.addWidget(self.valueLabel, 2, 0)
        gridLayout.addWidget(self.valueInput, 3, 0)

        gridLayout.setRowStretch(4, 1)
    
        gridLayout.addWidget(self.buttonRun, 5, 0)

        self.buttonRun.clicked.connect(self.onClickButtonRun)
        
        self.timer.timeout.connect(self.autoKeyHandler)

    def onClickButtonRun(self):

        if self.timer.isActive():
            self.intervalInput.setDisabled(False)
            self.valueInput.setDisabled(False)
            self.buttonRun.setText("Iniciar")
            self.timer.stop()
            return
        
        value = self.intervalInput.text()

        if value == '':
            QMessageBox.warning(self, 'Atenção', 'Insira um valor para o intervalo!')
            return

        interval = int(value) * 1000

        self.timer.start(interval)
        self.buttonRun.setText("Parar")
        self.intervalInput.setDisabled(True)
        self.valueInput.setDisabled(True)

    
    def autoKeyHandler(self):
        kbd.type(self.valueInput.text())


if __name__ == '__main__':

    import sys

    app = PyQt5.QtWidgets.QApplication(sys.argv)

    win = AppWidget()
    win.show()

    sys.exit(app.exec_())